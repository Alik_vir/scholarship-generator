package com.conf;

import java.sql.*;

public class DataBaseHandler {
    private static Connection dbConn;

    public static void openDbConn()throws ClassNotFoundException, SQLException {
        Config conf = new Config();
        String user = conf.getdbUser();
        String pass = conf.getdbPass();
        try { //загрузка драйвера
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {       //строка подключения  ?user=fred&password=secret&ssl=true

            dbConn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/lar", user, pass);
        } catch (SQLException e) {
            System.out.println("Connection Failed!");
            e.printStackTrace();
        }
        if (dbConn != null) {
            System.out.println("You made it, take control your database now!");
        } else {
            System.out.println("Failed to make connection!");
        }
    }

    public static Connection getDbConn() {
        return dbConn;
    }

        public static void closeDbConn(){
        try {
            dbConn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
