package com.conf;

public class Config {
    private static String dbUser;
    private static String dbPass;

    public String getdbUser() {
        return dbUser;
    }


    public void setdbUser(String a) {
        dbUser = a;
    }

    public String getdbPass() {
        return dbPass;
    }

    public void setdbPass(String b) {
        dbPass = b;
    }
}
