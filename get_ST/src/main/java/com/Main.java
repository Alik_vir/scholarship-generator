package com;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;



public class Main extends Application {

    public static Stage pStage;


    @Override
    public void start(Stage primaryStage) throws Exception {
        pStage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("/Windows/aut.fxml"));
        primaryStage.setTitle("Генератор стипендий");
        primaryStage.setScene(new Scene(root, 378, 332));
        primaryStage.show();
        primaryStage.setResizable(false);

    }



    public static void main(String[] args) {
        launch(args);
    }
}
