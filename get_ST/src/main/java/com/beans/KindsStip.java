package com.beans;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class KindsStip extends RecursiveTreeObject<KindsStip> {
    private final SimpleIntegerProperty num_stip;
    private final SimpleStringProperty kinds;
    private final SimpleIntegerProperty size_st;

    public KindsStip(Integer num_stip, String kinds, Integer size_st) {
        this.num_stip = new SimpleIntegerProperty(num_stip);
        this.kinds = new SimpleStringProperty(kinds);
        this.size_st = new SimpleIntegerProperty(size_st);
    }

    public int getNum_stip() {
        return num_stip.get();
    }

    public IntegerProperty num_stipProperty() {
        return num_stip;
    }

    public void setNum_stip(int num_stip) {
        this.num_stip.set(num_stip);
    }

    public String getKinds() {
        return kinds.get();
    }

    public StringProperty kindsProperty() {
        return kinds;
    }

    public void setKinds(String kinds) {
        this.kinds.set(kinds);
    }

    public int getSize_st() {
        return size_st.get();
    }

    public IntegerProperty size_stProperty() {
        return size_st;
    }

    public void setSize_st(int size_st) {
        this.size_st.set(size_st);
    }
}
