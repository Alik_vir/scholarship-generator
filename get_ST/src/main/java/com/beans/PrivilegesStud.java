package com.beans;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

import java.sql.Date;

public class PrivilegesStud extends RecursiveTreeObject<PrivilegesStud> {
    private final SimpleIntegerProperty id_pr;
    private final SimpleStringProperty surname;
    private final SimpleStringProperty name_stud;
    private final SimpleStringProperty id_group;
    private final SimpleStringProperty doc;
    private final SimpleObjectProperty<Date> date_pr;
    private final SimpleStringProperty notes;


    public PrivilegesStud(Integer id_pr, String surname, String name_stud, String id_group, String doc, Date date_pr, String notes) {
        this.id_pr = new SimpleIntegerProperty(id_pr);
        this.surname = new SimpleStringProperty(surname);
        this.name_stud = new SimpleStringProperty(name_stud);
        this.id_group = new SimpleStringProperty(id_group);
        this.doc = new SimpleStringProperty(doc);
        this.date_pr = new SimpleObjectProperty<Date>(date_pr);
        this.notes = new SimpleStringProperty(notes);
    }

    public int getId_pr() {
        return id_pr.get();
    }

    public SimpleIntegerProperty id_prProperty() {
        return id_pr;
    }

    public void setId_pr(int id_pr) {
        this.id_pr.set(id_pr);
    }

    public String getSurname() {
        return surname.get();
    }

    public SimpleStringProperty surnameProperty() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname.set(surname);
    }

    public String getName_stud() {
        return name_stud.get();
    }

    public SimpleStringProperty name_studProperty() {
        return name_stud;
    }

    public void setName_stud(String name_stud) {
        this.name_stud.set(name_stud);
    }

    public String getId_group() {
        return id_group.get();
    }

    public SimpleStringProperty id_groupProperty() {
        return id_group;
    }

    public void setId_group(String id_group) {
        this.id_group.set(id_group);
    }

    public String getDoc() {
        return doc.get();
    }

    public SimpleStringProperty docProperty() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc.set(doc);
    }

    public Date getDate_pr() {
        return date_pr.get();
    }

    public SimpleObjectProperty<Date> date_prProperty() {
        return date_pr;
    }

    public void setDate_pr(Date date_pr) {
        this.date_pr.set(date_pr);
    }

    public String getNotes() {
        return notes.get();
    }

    public SimpleStringProperty notesProperty() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes.set(notes);
    }
}
