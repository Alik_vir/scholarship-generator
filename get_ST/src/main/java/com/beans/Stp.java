package com.beans;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Stp extends RecursiveTreeObject<Stp> {
    private final SimpleIntegerProperty num;
    private final SimpleIntegerProperty num_jurnal;
    private final SimpleStringProperty surname;
    private final SimpleStringProperty name_stud;
    private final SimpleStringProperty sex;
    private final SimpleStringProperty id_group;
    private final SimpleStringProperty kinds;
    private final SimpleIntegerProperty size_st;
    private final SimpleIntegerProperty num_semestr;

    public Stp(Integer num, Integer num_jurnal, String surname, String name_stud, String sex, String id_group, String kinds, Integer size_st, Integer num_semestr) {
        this.num = new SimpleIntegerProperty(num);
        this.num_jurnal = new SimpleIntegerProperty(num_jurnal);
        this.surname = new SimpleStringProperty(surname);
        this.name_stud = new SimpleStringProperty(name_stud);
        this.sex = new SimpleStringProperty(sex);
        this.id_group = new SimpleStringProperty(id_group);
        this.kinds = new SimpleStringProperty(kinds);
        this.size_st = new SimpleIntegerProperty(size_st);
        this.num_semestr = new SimpleIntegerProperty(num_semestr);
    }

    public int getNum() {
        return num.get();
    }

    public SimpleIntegerProperty numProperty() {
        return num;
    }

    public void setNum(int num) {
        this.num.set(num);
    }

    public int getNum_jurnal() {
        return num_jurnal.get();
    }

    public SimpleIntegerProperty num_jurnalProperty() {
        return num_jurnal;
    }

    public void setNum_jurnal(int num_jurnal) {
        this.num_jurnal.set(num_jurnal);
    }

    public String getSurname() {
        return surname.get();
    }

    public SimpleStringProperty surnameProperty() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname.set(surname);
    }

    public String getName_stud() {
        return name_stud.get();
    }

    public SimpleStringProperty name_studProperty() {
        return name_stud;
    }

    public void setName_stud(String name_stud) {
        this.name_stud.set(name_stud);
    }

    public String getSex() {
        return sex.get();
    }

    public SimpleStringProperty sexProperty() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex.set(sex);
    }

    public String getId_group() {
        return id_group.get();
    }

    public SimpleStringProperty id_groupProperty() {
        return id_group;
    }

    public void setId_group(String id_group) {
        this.id_group.set(id_group);
    }

    public String getKinds() {
        return kinds.get();
    }

    public SimpleStringProperty kindsProperty() {
        return kinds;
    }

    public void setKinds(String kinds) {
        this.kinds.set(kinds);
    }

    public int getSize_st() {
        return size_st.get();
    }

    public SimpleIntegerProperty size_stProperty() {
        return size_st;
    }

    public void setSize_st(int size_st) {
        this.size_st.set(size_st);
    }

    public int getNum_semestr() {
        return num_semestr.get();
    }

    public SimpleIntegerProperty num_semestrProperty() {
        return num_semestr;
    }

    public void setNum_semestr(int num_semestr) {
        this.num_semestr.set(num_semestr);
    }
}
