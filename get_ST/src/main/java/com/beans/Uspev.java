package com.beans;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Uspev extends RecursiveTreeObject<Uspev> {
    private final SimpleIntegerProperty id_uspev;
    private final SimpleStringProperty surname;
    private final SimpleStringProperty name_stud;
    private final SimpleStringProperty sex;
    private final SimpleStringProperty id_group;
    private final SimpleIntegerProperty num_semestr;
    private final SimpleStringProperty name_discipline;
    private final SimpleIntegerProperty mark;

    public Uspev(Integer id_uspev, String surname, String name_stud, String sex, String id_group, Integer num_semestr, String name_discipline, Integer mark) {
        this.id_uspev = new SimpleIntegerProperty(id_uspev);
        this.surname = new SimpleStringProperty(surname);
        this.name_stud = new SimpleStringProperty(name_stud);
        this.sex = new SimpleStringProperty(sex);
        this.id_group = new SimpleStringProperty(id_group);
        this.num_semestr = new SimpleIntegerProperty(num_semestr);
        this.name_discipline = new SimpleStringProperty(name_discipline);
        this.mark = new SimpleIntegerProperty(mark);
    }

    public int getId_uspev() {
        return id_uspev.get();
    }

    public SimpleIntegerProperty id_uspevProperty() {
        return id_uspev;
    }

    public void setId_uspev(int id_uspev) {
        this.id_uspev.set(id_uspev);
    }

    public String getSurname() {
        return surname.get();
    }

    public SimpleStringProperty surnameProperty() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname.set(surname);
    }

    public String getName_stud() {
        return name_stud.get();
    }

    public SimpleStringProperty name_studProperty() {
        return name_stud;
    }

    public void setName_stud(String name_stud) {
        this.name_stud.set(name_stud);
    }

    public String getSex() {
        return sex.get();
    }

    public SimpleStringProperty sexProperty() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex.set(sex);
    }

    public String getId_group() {
        return id_group.get();
    }

    public SimpleStringProperty id_groupProperty() {
        return id_group;
    }

    public void setId_group(String id_group) {
        this.id_group.set(id_group);
    }

    public int getNum_semestr() {
        return num_semestr.get();
    }

    public SimpleIntegerProperty num_semestrProperty() {
        return num_semestr;
    }

    public void setNum_semestr(int num_semestr) {
        this.num_semestr.set(num_semestr);
    }

    public String getName_discipline() {
        return name_discipline.get();
    }

    public SimpleStringProperty name_disciplineProperty() {
        return name_discipline;
    }

    public void setName_discipline(String name_discipline) {
        this.name_discipline.set(name_discipline);
    }

    public int getMark() {
        return mark.get();
    }

    public SimpleIntegerProperty markProperty() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark.set(mark);
    }
}
