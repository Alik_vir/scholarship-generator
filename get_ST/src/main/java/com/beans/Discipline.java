package com.beans;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class Discipline extends RecursiveTreeObject<Discipline> {
    private final SimpleIntegerProperty id_discipline;
    private final SimpleStringProperty name_discipline;


    public Discipline(Integer id_discipline, String name_discipline){
        this.id_discipline = new SimpleIntegerProperty(id_discipline);
        this.name_discipline = new SimpleStringProperty(name_discipline);
    }

    public int getId_discipline() {
        return id_discipline.get();
    }

    public SimpleIntegerProperty id_disciplineProperty() {
        return id_discipline;
    }

    public void setId_discipline(int id_discipline) {
        this.id_discipline.set(id_discipline);
    }

    public String getName_discipline() {
        return name_discipline.get();
    }

    public SimpleStringProperty name_disciplineProperty() {
        return name_discipline;
    }

    public void setName_discipline(String name_discipline) {
        this.name_discipline.set(name_discipline);
    }
}

