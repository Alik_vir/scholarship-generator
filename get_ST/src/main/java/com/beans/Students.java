package com.beans;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

import java.sql.Date;

public class Students extends RecursiveTreeObject<Students> {
    private final SimpleIntegerProperty id_student;
    private final SimpleIntegerProperty num_jurnal;
    private final SimpleStringProperty surname;
    private final SimpleStringProperty name_stud;
    private final SimpleStringProperty sex;
    private final SimpleObjectProperty<Date> birthday;
    private final SimpleStringProperty phone;
    private final SimpleStringProperty id_group;



    public Students(Integer id_student, Integer nam_journal, String surname, String name_stud, String sex, Date birthday, String phone, String id_group) {
        this.id_student = new SimpleIntegerProperty(id_student);
        this.num_jurnal = new SimpleIntegerProperty(nam_journal);
        this.surname = new SimpleStringProperty(surname);
        this.name_stud = new SimpleStringProperty(name_stud);
        this.sex = new SimpleStringProperty(sex);
        this.birthday = new SimpleObjectProperty<Date>(birthday);
        this.phone = new SimpleStringProperty(phone);
        this.id_group = new SimpleStringProperty(id_group);
    }

    public int getId_student() {
        return id_student.get();
    }

    public SimpleIntegerProperty id_studentProperty() {
        return id_student;
    }

    public void setId_student(int id_student) {
        this.id_student.set(id_student);
    }

    public int getNum_jurnal() {
        return num_jurnal.get();
    }

    public SimpleIntegerProperty num_jurnalProperty() {
        return num_jurnal;
    }

    public void setNum_jurnal(int num_jurnal) {
        this.num_jurnal.set(num_jurnal);
    }

    public String getSurname() {
        return surname.get();
    }

    public SimpleStringProperty surnameProperty() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname.set(surname);
    }

    public String getName_stud() {
        return name_stud.get();
    }

    public SimpleStringProperty name_studProperty() {
        return name_stud;
    }

    public void setName_stud(String name_stud) {
        this.name_stud.set(name_stud);
    }

    public String getSex() {
        return sex.get();
    }

    public SimpleStringProperty sexProperty() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex.set(sex);
    }

    public Date getBirthday() {
        return birthday.get();
    }

    public SimpleObjectProperty<Date> birthdayProperty() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday.set(birthday);
    }

    public String getPhone() {
        return phone.get();
    }

    public SimpleStringProperty phoneProperty() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone.set(phone);
    }

    public String getId_group() {
        return id_group.get();
    }

    public SimpleStringProperty id_groupProperty() {
        return id_group;
    }

    public void setId_group(String id_group) {
        this.id_group.set(id_group);
    }
}

