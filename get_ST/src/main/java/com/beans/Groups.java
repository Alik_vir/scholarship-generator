package com.beans;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;

public class Groups extends RecursiveTreeObject<Groups> {


    private final SimpleStringProperty id_group;
    private final SimpleStringProperty id_spec;


    public Groups(String id_group, String id_spec) {
        this.id_group = new SimpleStringProperty(id_group);
        this.id_spec = new SimpleStringProperty(id_spec);
    }

    public String getId_group() {
        return id_group.get();
    }

    public SimpleStringProperty id_groupProperty() {
        return id_group;
    }

    public void setId_group(String id_group) {
        this.id_group.set(id_group);
    }

    public String getId_spec() {
        return id_spec.get();
    }

    public SimpleStringProperty id_specProperty() {
        return id_spec;
    }

    public void setId_spec(String id_spec) {
        this.id_spec.set(id_spec);
    }
}
