package com.beans;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Specialtonsti extends RecursiveTreeObject<Specialtonsti> {
    private StringProperty id_spec;
    private StringProperty name_spec;
    private SimpleStringProperty time_tutorial;

    public Specialtonsti(String id_spec, String name_spec, String time_tutorial) {
        this.id_spec = new SimpleStringProperty(id_spec);
        this.name_spec = new SimpleStringProperty(name_spec);
        this.time_tutorial = new SimpleStringProperty(time_tutorial);
    }

    public String getId_spec() {
        return id_spec.get();
    }

    public StringProperty id_specProperty() {
        return id_spec;
    }

    public void setId_spec(String id_spec) {
        this.id_spec.set(id_spec);
    }

    public String getName_spec() {
        return name_spec.get();
    }

    public StringProperty name_specProperty() {
        return name_spec;
    }

    public void setName_spec(String name_spec) {
        this.name_spec.set(name_spec);
    }

    public String getTime_tutorial() {
        return time_tutorial.get();
    }

    public SimpleStringProperty time_tutorialProperty() {
        return time_tutorial;
    }

    public void setTime_tutorial(String time_tutorial) {
        this.time_tutorial.set(time_tutorial);
    }


    @Override
    public String toString() {
        return "Specialtonsti{" +
                "id_spec=" + id_spec +
                ", name_spec=" + name_spec +
                ", time_tutorial=" + time_tutorial +
                '}';

    }
}
