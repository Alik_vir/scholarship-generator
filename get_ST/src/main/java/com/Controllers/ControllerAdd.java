package com.Controllers;

import com.Main;
import com.beans.Stp;
import com.conf.DataBaseHandler;
import com.jfoenix.controls.*;

import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;
import com.beans.Specialtonsti;
import com.jfoenix.controls.events.JFXDialogEvent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.ToggleGroup;
import javafx.scene.effect.BoxBlur;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import org.postgresql.util.PSQLException;

import static com.conf.DataBaseHandler.getDbConn;

public class ControllerAdd {


    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private MenuItem change;

    @FXML
    private MenuItem exit;

    @FXML
    private MenuItem about;

    @FXML
    private JFXTextField specShifr;

    @FXML
    private JFXButton specBtn;

    @FXML
    private JFXTextField specName;

    @FXML
    private JFXTextField specTime;

    @FXML
    private JFXTextField groupNum;

    @FXML
    private JFXButton groupBtn;

    @FXML
    private JFXComboBox groupSpec;

    @FXML
    private JFXTextField discNum;

    @FXML
    private JFXButton discBtn;

    @FXML
    private JFXTextField discName;

    @FXML
    private JFXTextField studJur;

    @FXML
    private JFXButton studBtn;

    @FXML
    private JFXTextField studSurn;

    @FXML
    private JFXTextField studName;

    @FXML
    private JFXTextField studPhone;

    @FXML
    private JFXDatePicker studBirth;

    @FXML
    private JFXRadioButton studMale;

    @FXML
    private VBox vbox;

    @FXML
    private ToggleGroup sex;

    @FXML
    private JFXRadioButton studFamale;

    @FXML
    private JFXComboBox stGroup;

    @FXML
    private JFXComboBox privNumstud;

    @FXML
    private JFXTextField privDoc;

    @FXML
    private JFXDatePicker privDate;

    @FXML
    private JFXTextArea privNotes;

    @FXML
    private JFXButton privBtn;

    @FXML
    private JFXTextField kinStep;

    @FXML
    private JFXButton kinBtn;

    @FXML
    private JFXTextField kinSize;

    @FXML
    private JFXTextField uspevSemestr;

    @FXML
    private JFXButton uspevBtn;

    @FXML
    private JFXTextField uspevDisc;

    @FXML
    private JFXTextField uspevStud;

    @FXML
    private JFXComboBox uspevMark;

    @FXML
    private JFXTextField stpStud;

    @FXML
    private JFXButton stpBtn;

    @FXML
    private JFXTextField stpStipend;

    @FXML
    private JFXTextField stpSemestr;

    @FXML
    private AnchorPane studAnchor;

    @FXML
    private AnchorPane grAnchor;

    @FXML
    private AnchorPane kindsAnchor;

    @FXML
    private StackPane rootPane;

    @FXML
    private AnchorPane rootAnchore;

    @FXML
    void initialize() throws SQLException {

        specBtn.setOnAction(event -> {//специальности
            String add = "select c_spec('"+ specShifr.getText().trim() +"','"+ specName.getText().trim() +"','"+ specTime.getText().trim() +"');";
            adding(add);

            specName.clear();
            specTime.clear();
            specShifr.clear();
        });
        privBtn.setOnAction(event -> {//самое кривое добвление которое я видел, но работает так что пох, Льготы
            String add= "select c_privil('"+ privNumstud.getValue() +"', '"+ privDoc.getText().trim() +"', '"+privDate.getValue()+"', '"+privNotes.getText().trim()+"');";
            adding(add);

            privNumstud.setValue(null);
            privDoc.clear();
            privDate.setValue(null);
            privNotes.clear();
        });
        groupBtn.setOnAction(event -> {
            String add = "select c_groups('"+groupNum.getText().trim()+"','"+groupSpec.getValue()+"');";
            adding(add);

            groupNum.clear();
            groupSpec.setValue(null);
        });
        discBtn.setOnAction(event -> {
            String add = "select c_discipline('"+discName.getText().trim()+"');";
            adding(add);

            discName.clear();
        });
        studBtn.setOnAction(event -> {
            String sex = null;
            if(studMale.isSelected())
            {
                sex = "М";
            }
            else if(studFamale.isSelected())
            {
                sex = "Ж";
            } else sex = "Ч";

            String add = "select c_students('"+studJur.getText().trim()+"', '"+ studSurn.getText().trim()+"', '"+studName.getText().trim()+"','"+sex+"', '"+studBirth.getValue()+"', '"+studPhone.getText().trim()+"', '"+stGroup.getValue()+"');";
            adding(add);

            studJur.setText(null);
            studSurn.clear();
            studName.clear();
            studBirth.setValue(null);
            studPhone.clear();
            stGroup.setValue(null);
        });
        kinBtn.setOnAction(event -> {
            String add = "select c_stip('"+kinStep.getText().trim()+"','"+kinSize.getText().trim()+"');";
            adding(add);

            kinStep.clear();
            kinSize.clear();
        });
        uspevBtn.setOnAction(event -> {
            String add = "select c_uspev('"+uspevSemestr.getText().trim()+"','"+uspevDisc.getText().trim()+"','"+uspevStud.getText().trim()+"','"+uspevMark.getValue()+"');";
            adding(add);

            uspevSemestr.clear();
            uspevDisc.clear();
            uspevStud.clear();
            uspevMark.setValue(null);
        });

        change.setOnAction(event -> {
            Scene scene = null;
            try
            {
                scene = new Scene(FXMLLoader.load(getClass().getResource("/Windows/tables.fxml")));
                Main.pStage.setScene(scene);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        });

        exit.setOnAction(event -> {
            System.exit(0);
        });
        about.setOnAction(event -> {
            dialog("О программе","Данная программа была разработа исключительно в целях учебы.\n" +
                                                "На данном окне вы можете добавить данные в таблицы, \n" +
                                                "названия наблиц находятся на вкладках этого окна.\n" +
                                                "Спасибо за то что пользуетесь моей программой.\n" +
                                                "Автор: человек.");
        });

        fillGroup();
        fillStud();
        fillKinds();
        fillMark();
    }

    private void adding (String add)  {
        String adding = add;
        Statement st = null;
        try {
            st = getDbConn().createStatement();
            st.execute(adding);
            dialog("Выполнено!","Запись была добавлена");
        } catch (SQLException e) {
            e.printStackTrace();
            dialog("Ошбика!","Ошибка при добавлении записи.\n" +
                    "Проверьте правильность введенных вами данных.\n" +
                    "Если проблема не исчезнет, обратитесь к администратору.");
        }
    }

    private void dialog(String heading,String body){
        BoxBlur blur = new BoxBlur(3, 3, 3);

        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setHeading(new Text(heading));
        dialogLayout.setBody(new Text(body));
        JFXButton btn = new JFXButton("ОК");
        btn.getStyleClass().add("dialog-button");
        JFXDialog dialog = new JFXDialog(rootPane, dialogLayout, JFXDialog.DialogTransition.TOP);
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialog.close();
            }
        });
        dialogLayout.setActions(btn);
        dialog.show();
        dialog.setOnDialogClosed((JFXDialogEvent event)->{
            rootAnchore.setEffect(null);
        });
        rootAnchore.setEffect(blur);
    }

    private void fillMark(){
        uspevMark.getItems().addAll("1","2","3","4","5");
    }

    private void fillKinds() throws SQLException {
        ObservableList list = FXCollections.observableArrayList();

        kindsAnchor.getChildren().add(privNumstud);
        privNumstud.setItems(list);
        privNumstud.setLayoutX(70);
        privNumstud.setLayoutY(38);

        String fill = "select id_student from students;";

        Connection connection = getDbConn();
        PreparedStatement statement = connection.prepareStatement(fill);
        ResultSet rs = statement.executeQuery();

        while (rs.next()){
            list.add(rs.getString("id_student"));
        }
        statement.close();
        rs.close();

    }

    private void fillGroup() throws SQLException{
    ObservableList list = FXCollections.observableArrayList();

    grAnchor.getChildren().add(groupSpec);
    groupSpec.setItems(list);
    groupSpec.setLayoutX(217);
    groupSpec.setLayoutY(131);

    String fill = "select id_spec from specialnosti;";

    Connection connection = getDbConn();
    PreparedStatement statement = connection.prepareStatement(fill);
    ResultSet rs = statement.executeQuery();

    while (rs.next()){
        list.add(rs.getString("id_spec"));
    }
    statement.close();
    rs.close();
}

    private void fillStud()  throws SQLException {
        ObservableList list = FXCollections.observableArrayList();

        studAnchor.getChildren().add(stGroup);
        stGroup.setItems(list);
        stGroup.setLayoutX(99);
        stGroup.setLayoutY(188);

        String fill = "select id_group from groups;";

        Connection connection = getDbConn();
        PreparedStatement statement = connection.prepareStatement(fill);
        ResultSet rs = statement.executeQuery();

        while (rs.next()){
            list.add(rs.getString("id_group"));
        }
        statement.close();
        rs.close();

    }


}
