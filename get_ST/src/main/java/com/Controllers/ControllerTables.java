package com.Controllers;

import com.Main;
import com.beans.*;
import com.conf.Config;
import com.jfoenix.controls.*;
import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import com.jfoenix.controls.events.JFXDialogEvent;
import com.jfoenix.transitions.hamburger.HamburgerNextArrowBasicTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.effect.BoxBlur;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import static com.conf.DataBaseHandler.getDbConn;


public class ControllerTables {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private MenuItem stGen;

    @FXML
    private MenuItem exit;

    @FXML
    private MenuItem listing_m;

    @FXML
    private MenuItem addBtn;

    @FXML
    private MenuItem editBtn;

    @FXML
    private MenuItem delBtn;

    @FXML
    private MenuItem about;

    @FXML
    private MenuItem admin;

    @FXML
    private JFXTreeTableView<Specialtonsti> spec_table;

    @FXML
    private JFXTextField input;

    @FXML
    private JFXComboBox combBox;

    @FXML
    private JFXTreeTableView<Groups> groupTab;

    @FXML
    private JFXTextField inputGroup;

    @FXML
    private JFXComboBox comboGroup;

    @FXML
    private JFXTreeTableView<Discipline> discTab;

    @FXML
    private JFXTextField inputDisc;

    @FXML
    private JFXComboBox combDisc;

    @FXML
    private JFXTreeTableView<Students> studentTab;

    @FXML
    private JFXTextField inputStudent;

    @FXML
    private JFXComboBox combStudent;

    @FXML
    private JFXTreeTableView<PrivilegesStud> privilegesTab;

    @FXML
    private JFXTextField inputPrivileges;

    @FXML
    private JFXComboBox combPrivileges;

    @FXML
    private JFXTreeTableView<KindsStip> kindsTab;

    @FXML
    private JFXTextField inputKinds;

    @FXML
    private JFXComboBox combKinds;

    @FXML
    private JFXTreeTableView<Uspev> uspevTab;

    @FXML
    private JFXTextField inputUspev;

    @FXML
    private JFXComboBox combUspev;

    @FXML
    private JFXTreeTableView<Stp> stTab;

    @FXML
    private JFXTextField inputSt;

    @FXML
    private JFXComboBox combSt;

    @FXML
    private JFXHamburger humburger;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Tab spectab;

    @FXML
    private Tab groutab;

    @FXML
    private Tab distab;

    @FXML
    private Tab studtab;

    @FXML
    private Tab privtab;

    @FXML
    private Tab kindstab;

    @FXML
    private Tab uspevtab;

    @FXML
    private StackPane mainStack;

    @FXML
    private AnchorPane anchorePaneMain;


    private void list_SPEC(){
        JFXTreeTableColumn<Specialtonsti, String> id_spec = new JFXTreeTableColumn<>("Шифр");
        JFXTreeTableColumn<Specialtonsti, String> name_spec = new JFXTreeTableColumn<>("Наименование специальности");
        JFXTreeTableColumn<Specialtonsti, String> time_tutorial = new JFXTreeTableColumn<>("Время обучения");

        id_spec.setPrefWidth(150);
        name_spec.setPrefWidth(250);

        id_spec.setCellValueFactory(new TreeItemPropertyValueFactory<Specialtonsti, String>("id_spec"));
        name_spec.setCellValueFactory(new TreeItemPropertyValueFactory<Specialtonsti, String>("name_spec"));
        time_tutorial.setCellValueFactory(new TreeItemPropertyValueFactory<Specialtonsti, String>("time_tutorial"));


        ObservableList<Specialtonsti> list = FXCollections.observableArrayList();
        try {
            Connection connection = getDbConn();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM specialnosti;");
            ResultSet rs = statement.executeQuery();
            while (rs.next())
                list.add(new Specialtonsti(rs.getString(1), rs.getString(2), rs.getString(3)));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final TreeItem<Specialtonsti> root = new RecursiveTreeItem<Specialtonsti>(list, RecursiveTreeObject :: getChildren);
        spec_table.getColumns().setAll(id_spec, name_spec, time_tutorial);
        spec_table.setRoot(root);
        spec_table.setShowRoot(false);

        input.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                spec_table.setPredicate(new Predicate<TreeItem<Specialtonsti>>() {
                    @Override
                    public boolean test(TreeItem<Specialtonsti> spec) {
                        if(combBox.getValue().equals("Шифр")){
                            Boolean flag = spec.getValue().id_specProperty().getValue().contains(newValue);
                            return flag;
                        }else {
                            Boolean flag2 = spec.getValue().name_specProperty().getValue().contains(newValue);
                            return flag2;
                        }
                    }
                });
            }
        }); //вариация поиска
    }

    private void list_GROUP(){


        JFXTreeTableColumn<Groups, String> id_group = new JFXTreeTableColumn<>("Номер группы");
        JFXTreeTableColumn<Groups, String> id_spec = new JFXTreeTableColumn<>("Номер специальности");

        id_spec.setPrefWidth(150);
        id_group.setPrefWidth(150);


        id_group.setCellValueFactory(new TreeItemPropertyValueFactory<Groups, String>("id_group"));
        id_spec.setCellValueFactory(new TreeItemPropertyValueFactory<Groups, String>("id_spec"));

        ObservableList<Groups> list = FXCollections.observableArrayList();
        try {
            Connection connection = getDbConn();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM groups;");
            ResultSet rs = statement.executeQuery();
            while (rs.next())
                list.add(new Groups(rs.getString(1), rs.getString(2)));
        } catch (SQLException e) {
            e.printStackTrace();
        }



        final TreeItem<Groups> root = new RecursiveTreeItem<Groups>(list, RecursiveTreeObject :: getChildren);
        groupTab.getColumns().setAll(id_group, id_spec);
        groupTab.setRoot(root);
        groupTab.setShowRoot(false);

        inputGroup.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                groupTab.setPredicate(new Predicate<TreeItem<Groups>>() {
                    @Override
                    public boolean test(TreeItem<Groups> list) {
                        Boolean flag = list.getValue().id_groupProperty().getValue().contains(newValue);
                        return flag;
                    }
                });
            }
        }); //вариация поиска

    }

    private void list_DISC(){

        JFXTreeTableColumn id_discipline = new JFXTreeTableColumn("Номер");
        JFXTreeTableColumn name_discipline = new JFXTreeTableColumn("Название дициплины");


        id_discipline.setCellValueFactory(new TreeItemPropertyValueFactory<Discipline, Integer>("id_discipline"));
        name_discipline.setCellValueFactory(new TreeItemPropertyValueFactory<Discipline, String>("name_discipline"));

        discTab.getColumns().setAll(id_discipline, name_discipline);

        ObservableList<Discipline> list = FXCollections.observableArrayList();
        try {
            Connection connection = getDbConn();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM discipline;");
            ResultSet rs = statement.executeQuery();
            while (rs.next())
                list.add(new Discipline(rs.getInt(1), rs.getString(2)));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        TreeItem<Discipline> root = new RecursiveTreeItem<Discipline>(list, RecursiveTreeObject :: getChildren);

        discTab.setRoot(root);
        discTab.setShowRoot(false);
        inputDisc.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                discTab.setPredicate(new Predicate<TreeItem<Discipline>>() {
                    @Override
                    public boolean test(TreeItem<Discipline> list) {
                        Boolean flag = list.getValue().name_disciplineProperty().getValue().contains(newValue);
                        return flag;
                    }
                });
            }
        }); //вариация поиска

    }

    private void list_STUDENT(){
        JFXTreeTableColumn id_student = new JFXTreeTableColumn("Номер");
        JFXTreeTableColumn num_jurnal = new JFXTreeTableColumn("Журнал");
        JFXTreeTableColumn surname = new JFXTreeTableColumn("Фамилия");
        JFXTreeTableColumn name_stud = new JFXTreeTableColumn("Имя");
        JFXTreeTableColumn sex = new JFXTreeTableColumn("Пол");
        JFXTreeTableColumn birthday = new JFXTreeTableColumn("Дата рождения");
        JFXTreeTableColumn phone = new JFXTreeTableColumn("Телефон");
        JFXTreeTableColumn id_group = new JFXTreeTableColumn("Группа");


        id_student.setCellValueFactory(new TreeItemPropertyValueFactory<Students, Integer>("id_student"));
        num_jurnal.setCellValueFactory(new TreeItemPropertyValueFactory<Students, Integer>("num_jurnal"));
        surname.setCellValueFactory(new TreeItemPropertyValueFactory<Students, String>("surname"));
        name_stud.setCellValueFactory(new TreeItemPropertyValueFactory<Students, String>("name_stud"));
        sex.setCellValueFactory(new TreeItemPropertyValueFactory<Students, String>("sex"));
        birthday.setCellValueFactory(new TreeItemPropertyValueFactory<Students, Date>("birthday"));
        phone.setCellValueFactory(new TreeItemPropertyValueFactory<Students, Integer>("phone"));
        id_group.setCellValueFactory(new TreeItemPropertyValueFactory<Students, String>("id_group"));

        ObservableList<Students> list = FXCollections.observableArrayList();

        try {
            Connection connection = getDbConn();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM students;");
            ResultSet rs = statement.executeQuery();
            while (rs.next())
                list.add(new Students(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getDate(6), rs.getString(7), rs.getString(8)));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final TreeItem<Students> root = new RecursiveTreeItem<Students>(list, RecursiveTreeObject :: getChildren);

        studentTab.getColumns().setAll(id_student, num_jurnal, surname, name_stud, sex, birthday, phone, id_group);
        studentTab.setRoot(root);
        studentTab.setShowRoot(false);
        inputStudent.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                studentTab.setPredicate(new Predicate<TreeItem<Students>>() {
                    @Override
                    public boolean test(TreeItem<Students> list) {
                        if(combStudent.getValue().equals("Фамилия")){
                            Boolean flag = list.getValue().surnameProperty().getValue().contains(newValue);
                            return flag;
                        } else  {
                            Boolean flag = list.getValue().id_groupProperty().getValue().contains(newValue);
                            return flag;
                        }
                    }
                });
            }
        });
    }

    private void list_PRIVILEGES(){

        JFXTreeTableColumn id_privileges = new JFXTreeTableColumn("№");
        JFXTreeTableColumn surname = new JFXTreeTableColumn("Фамилия");
        JFXTreeTableColumn name_stud = new JFXTreeTableColumn("Имя");
        JFXTreeTableColumn id_group = new JFXTreeTableColumn("Группа");
        JFXTreeTableColumn documents_for_privileges = new JFXTreeTableColumn("Документ");
        JFXTreeTableColumn date_privilages = new JFXTreeTableColumn("Дата");
        JFXTreeTableColumn notes = new JFXTreeTableColumn("Примечания");

        id_privileges.setCellValueFactory(new TreeItemPropertyValueFactory<Students, Integer>("id_pr"));
        surname.setCellValueFactory(new TreeItemPropertyValueFactory<Students, String>("surname"));
        name_stud.setCellValueFactory(new TreeItemPropertyValueFactory<Students, String>("name_stud"));
        id_group.setCellValueFactory(new TreeItemPropertyValueFactory<Students, String>("id_group"));
        documents_for_privileges.setCellValueFactory(new TreeItemPropertyValueFactory<Students, String>("doc"));
        date_privilages.setCellValueFactory(new TreeItemPropertyValueFactory<Students, Date>("date_pr"));
        notes.setCellValueFactory(new TreeItemPropertyValueFactory<Students, String>("notes"));

        ObservableList<PrivilegesStud> list = FXCollections.observableArrayList();

        try {
            Connection connection = getDbConn();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM goodprivileges;");
            ResultSet rs = statement.executeQuery();
            while (rs.next())
                list.add(new PrivilegesStud(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getDate(6), rs.getString(7)));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final TreeItem<PrivilegesStud> root = new RecursiveTreeItem<PrivilegesStud>(list, RecursiveTreeObject :: getChildren);
        privilegesTab.getColumns().setAll(id_privileges, surname, name_stud, id_group, documents_for_privileges, date_privilages, notes);
        privilegesTab.setRoot(root);
        privilegesTab.setShowRoot(false);

        inputPrivileges.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                privilegesTab.setPredicate(new Predicate<TreeItem<PrivilegesStud>>() {
                    @Override
                    public boolean test(TreeItem<PrivilegesStud> list) {
                        if(combPrivileges.getValue().equals("Фамилия")){
                            Boolean flag = list.getValue().surnameProperty().getValue().contains(newValue);
                            return flag;
                        } else  {
                            Boolean flag = list.getValue().id_groupProperty().getValue().contains(newValue);
                            return flag;
                        }
                    }
                });
            }
        });
    }

    private void list_KINDS(){


        JFXTreeTableColumn num_stip = new JFXTreeTableColumn("№");
        JFXTreeTableColumn kinds_stip = new JFXTreeTableColumn("Вид стипендии");
        JFXTreeTableColumn size_stip = new JFXTreeTableColumn("Размер");

        num_stip.setCellValueFactory(new TreeItemPropertyValueFactory<Students, Integer>("num_stip"));
        kinds_stip.setCellValueFactory(new TreeItemPropertyValueFactory<Students, String>("kinds"));
        size_stip.setCellValueFactory(new TreeItemPropertyValueFactory<Students, Integer>("size_st"));

        ObservableList<KindsStip> list = FXCollections.observableArrayList();
        try {
            Connection connection = getDbConn();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM kinds_stip;");
            ResultSet rs = statement.executeQuery();
            while (rs.next())
                list.add(new KindsStip(rs.getInt(1), rs.getString(2), rs.getInt(3)));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        final TreeItem<KindsStip> root = new RecursiveTreeItem<KindsStip>(list, RecursiveTreeObject :: getChildren);
        kindsTab.getColumns().setAll(num_stip, kinds_stip, size_stip);
        kindsTab.setRoot(root);
        kindsTab.setShowRoot(false);
        inputKinds.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                kindsTab.setPredicate(new Predicate<TreeItem<KindsStip>>() {
                    @Override
                    public boolean test(TreeItem<KindsStip> list) {
                        Boolean flag = list.getValue().kindsProperty().getValue().contains(newValue);
                        return flag;

                    }
                });
            }
        });
    }

    private void list_USPEV(){
        JFXTreeTableColumn id_uspev = new JFXTreeTableColumn("№");
        JFXTreeTableColumn sur_uspev = new JFXTreeTableColumn("Фамилия");
        JFXTreeTableColumn name_uspev = new JFXTreeTableColumn("Имя");
        JFXTreeTableColumn sex_uspev = new JFXTreeTableColumn("Пол");
        JFXTreeTableColumn group_uspev = new JFXTreeTableColumn("Группа");
        JFXTreeTableColumn semestr_uspev = new JFXTreeTableColumn("Семестр");
        JFXTreeTableColumn disciplin_uspev = new JFXTreeTableColumn("Дисциплина");
        JFXTreeTableColumn mark_uspev = new JFXTreeTableColumn("Оценка");

        ObservableList<Uspev> list = FXCollections.observableArrayList();
        id_uspev.setCellValueFactory(new TreeItemPropertyValueFactory<Uspev, Integer>("id_uspev"));
        sur_uspev.setCellValueFactory(new TreeItemPropertyValueFactory<Uspev, String>("surname"));
        name_uspev.setCellValueFactory(new TreeItemPropertyValueFactory<Uspev, String>("name_stud"));
        sex_uspev.setCellValueFactory(new TreeItemPropertyValueFactory<Uspev, String>("sex"));
        group_uspev.setCellValueFactory(new TreeItemPropertyValueFactory<Uspev, String>("id_group"));
        semestr_uspev.setCellValueFactory(new TreeItemPropertyValueFactory<Uspev, Integer>("num_semestr"));
        disciplin_uspev.setCellValueFactory(new TreeItemPropertyValueFactory<Uspev, String>("name_discipline"));
        mark_uspev.setCellValueFactory(new TreeItemPropertyValueFactory<Uspev, Integer>("mark"));
        try {
            Connection connection = getDbConn();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM gooduspev;");
            ResultSet rs = statement.executeQuery();
            while (rs.next())
                list.add(new Uspev(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getInt(6),  rs.getString(7), rs.getInt(8)));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        final TreeItem<Uspev> root = new RecursiveTreeItem<Uspev>(list, RecursiveTreeObject :: getChildren);
        uspevTab.getColumns().setAll(id_uspev, sur_uspev, name_uspev,sex_uspev, group_uspev, semestr_uspev,disciplin_uspev, mark_uspev);
        uspevTab.setRoot(root);
        uspevTab.setShowRoot(false);

        inputUspev.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                uspevTab.setPredicate(new Predicate<TreeItem<Uspev>>() {
                    @Override
                    public boolean test(TreeItem<Uspev> list) {
                        if(combUspev.getValue().equals("Фамилия")){
                            Boolean flag = list.getValue().surnameProperty().getValue().contains(newValue);
                            return flag;
                        } else if(combUspev.getValue().equals("Группа")){
                            Boolean flag = list.getValue().id_groupProperty().getValue().contains(newValue);
                            return flag;
                        }else{
                            Boolean flag = list.getValue().name_disciplineProperty().getValue().contains(newValue);
                            return flag;
                        }
                    }
                });
            }
        });
    }

    private void list_STP() {

        JFXTreeTableColumn num = new JFXTreeTableColumn("№");
        JFXTreeTableColumn num_jurnal = new JFXTreeTableColumn("Журнал");
        JFXTreeTableColumn surname = new JFXTreeTableColumn("Фамилия");
        JFXTreeTableColumn name_stud = new JFXTreeTableColumn("Имя");
        JFXTreeTableColumn sex = new JFXTreeTableColumn("Пол");
        JFXTreeTableColumn id_group = new JFXTreeTableColumn("Группа");
        JFXTreeTableColumn kinds = new JFXTreeTableColumn("Вид");
        JFXTreeTableColumn size_st = new JFXTreeTableColumn("Размер");
        JFXTreeTableColumn num_semestr = new JFXTreeTableColumn("Семестр");



        ObservableList<Stp> list = FXCollections.observableArrayList();
        num.setCellValueFactory(new TreeItemPropertyValueFactory<Stp, Integer>("num"));
        num_jurnal.setCellValueFactory(new TreeItemPropertyValueFactory<Stp, Integer>("num_jurnal"));
        surname.setCellValueFactory(new TreeItemPropertyValueFactory<Stp, String>("surname"));
        name_stud.setCellValueFactory(new TreeItemPropertyValueFactory<Stp, String>("name_stud"));
        sex.setCellValueFactory(new TreeItemPropertyValueFactory<Stp, String>("sex"));
        id_group.setCellValueFactory(new TreeItemPropertyValueFactory<Stp, String>("id_group"));
        kinds.setCellValueFactory(new TreeItemPropertyValueFactory<Stp, String>("kinds"));
        size_st.setCellValueFactory(new TreeItemPropertyValueFactory<Stp, Integer>("size_st"));
        num_semestr.setCellValueFactory(new TreeItemPropertyValueFactory<Stp, Integer>("num_semestr"));
        try {
            Connection connection = getDbConn();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM goodstp");
            ResultSet rs = statement.executeQuery();
            while (rs.next())
                list.add(new Stp(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getInt(8), rs.getInt(9)));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        final TreeItem<Stp> root = new RecursiveTreeItem<Stp>(list, RecursiveTreeObject :: getChildren);
        stTab.getColumns().setAll(num, num_jurnal, surname, name_stud, sex, id_group, kinds, size_st, num_semestr);
        stTab.setRoot(root);
        stTab.setShowRoot(false);

        inputSt.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                stTab.setPredicate(new Predicate<TreeItem<Stp>>() {
                    @Override
                    public boolean test(TreeItem<Stp> list) {
                        if(combSt.getValue().equals("Фамилия")){
                            Boolean flag = list.getValue().surnameProperty().getValue().contains(newValue);
                            return flag;
                        } else  {
                            Boolean flag = list.getValue().id_groupProperty().getValue().contains(newValue);
                            return flag;
                        }
                    }
                });
            }
        });
        combSt.setItems(list);

    }

    @FXML
    void initialize() {
        Config conf = new Config();
        fillingList();
        stGen.setOnAction(event -> {
            generate();
        });

        if(conf.getdbUser().equals("postgres")){
            admin.setVisible(true);
        }else{
            admin.setVisible(false);
        } //крч если ты зайдешь как админ то сможешь зайти в админку

        exit.setOnAction(event -> {
            System.exit(0);
        });

        about.setOnAction(event -> {
            //в доработке
//            controllerAdd.dialog("О программе","Данная программа была разработа исключительно в целях учебы.\n" +
//                    "На данном окне вы можете добавить данные в таблицы, \n" +
//                    "названия наблиц находятся на вкладках этого окна.\n" +
//                    "Спасибо за то что пользуетесь моей программой.\n" +
//                    "Автор: человек.");
        });

        admin.setOnAction(event -> {
            Scene admin = null;
            try
            {
                admin = new Scene(FXMLLoader.load(getClass().getResource("/Windows/adminPane.fxml")));
                Main.pStage.setScene(admin);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        });

        addBtn.setOnAction(event -> {
            Scene add = null;
            try
            {
                add = new Scene(FXMLLoader.load(getClass().getResource("/Windows/add.fxml")));
                Main.pStage.setScene(add);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        });

        listing_m.setOnAction(event -> {
            list_STP();
            list_USPEV();
            list_KINDS();
            list_PRIVILEGES();
            list_STUDENT();
            list_DISC();
            list_SPEC();
            list_GROUP();
        });

        delBtn.setOnAction(event -> {
            deleteSpec();
        });


        initDrawer();
    }

    private void deleteSpec(){
        Statement st = null;
            String del = null;
        if(spectab.isSelected()){
            del = "select d_spec('"+spec_table.getSelectionModel().getSelectedCells().get(0).getTreeItem().getValue().getId_spec()+"');";
        } else if(groutab.isSelected()){
            del = "select d_groups('"+groupTab.getSelectionModel().getSelectedCells().get(0).getTreeItem().getValue().getId_group()+"');";
        } else if(distab.isSelected()){
            del = "select d_disc('"+discTab.getSelectionModel().getSelectedCells().get(0).getTreeItem().getValue().getId_discipline()+"');";
        } else if(studtab.isSelected()){
            del = "select d_students('"+studentTab.getSelectionModel().getSelectedCells().get(0).getTreeItem().getValue().getId_student()+"');";
        } else if(privtab.isSelected()){
            del = "select d_privil('"+privilegesTab.getSelectionModel().getSelectedCells().get(0).getTreeItem().getValue().getId_pr()+"');";
        } else if(kindstab.isSelected()){
            del = "select d_stip('"+kindsTab.getSelectionModel().getSelectedCells().get(0).getTreeItem().getValue().getNum_stip()+"');";
        } else if(uspevtab.isSelected()){
            del = "select d_uspev('"+uspevTab.getSelectionModel().getSelectedCells().get(0).getTreeItem().getValue().getId_uspev()+"');";
        }
        try {
            st = getDbConn().createStatement();
            st.execute(del);
        } catch (SQLException e) {
            e.printStackTrace();
            dialog("ОШИБКА","Ошибка при удалении, попробуйте для начала обновить таблицу.\n" +
                    "При невозможности исправить ошибку, обратитесь к администратору.");
        }
    }



    private void generate(){
        String regStr = "select step_gen(1);";
        try {
            CallableStatement prSt = getDbConn().prepareCall(regStr);
            prSt.execute();

        } catch (SQLException e) {
            System.out.println("SQL exception");
            e.printStackTrace();
        }
    }

    private void fillingList() {
        combBox.getItems().addAll("Шифр","Наименование");
        combStudent.getItems().addAll("Фамилия","Группа");
        combPrivileges.getItems().addAll("Фамилия","Группа");
        combUspev.getItems().addAll("Фамилия","Группа","Дисциплина");
        combSt.getItems().addAll("Фамилия","Группа");
    }

    private void initDrawer() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/Windows/toolbar.fxml"));
            VBox toolbar = loader.load();
            drawer.setSidePane(toolbar);

//            for (Node node : toolbar.getChildren()){
//                if(node.getAccessibleText()!=null){
//                    node.addEventHandler(MouseEvent.MOUSE_CLICKED, (event -> {
//                        switch (node.getAccessibleText()) {
//                            case "exit":
//                                list_GROUP();
//
//                        }
//                    }));
//                }
//            }


        } catch (IOException ex) {
            Logger.getLogger(ControllerTables.class.getName()).log(Level.SEVERE, null, ex);
        }
        HamburgerNextArrowBasicTransition task = new HamburgerNextArrowBasicTransition(humburger);
        task.setRate(-1);
        humburger.addEventHandler(MouseEvent.MOUSE_CLICKED, (Event event) -> {
            drawer.toggle();
        });
        drawer.setOnDrawerOpening((event) -> {
            task.setRate(task.getRate() * -1);
            task.play();
            drawer.toFront();
        });
        drawer.setOnDrawerClosed((event) -> {
            drawer.toBack();
            task.setRate(task.getRate() * -1);
            task.play();
        });
    }

    private void dialog(String heading,String body){
        BoxBlur blur = new BoxBlur(3, 3, 3);

        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setHeading(new Text(heading));
        dialogLayout.setBody(new Text(body));
        JFXButton btn = new JFXButton("ОК");
        btn.getStyleClass().add("dialog-button");
        JFXDialog dialog = new JFXDialog(mainStack, dialogLayout, JFXDialog.DialogTransition.TOP);
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialog.close();
            }
        });
        dialogLayout.setActions(btn);
        dialog.show();
        dialog.setOnDialogClosed((JFXDialogEvent event)->{
            anchorePaneMain.setEffect(null);
        });
        anchorePaneMain.setEffect(blur);
    }
}
