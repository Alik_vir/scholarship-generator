package com.Controllers;

import com.Main;
import com.conf.Config;
import com.conf.DataBaseHandler;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;

public class ControllerAut {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private JFXButton enter_btn;

    @FXML
    private JFXTextField user_box;

    @FXML
    private JFXPasswordField pass_box;

    @FXML
    private FontAwesomeIconView lock_im;

    @FXML
    void initialize() {
        enter_btn.setOnAction(event -> {
            String user2 = user_box.getText().trim();
            String pass2 = pass_box.getText().trim();

            Config conf = new Config();
            conf.setdbPass(pass2);
            conf.setdbUser(user2);


            try {
                DataBaseHandler.openDbConn();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            Connection connection = DataBaseHandler.getDbConn();
            if (connection == null) {
                loginUserError();
            } else
            {
                Scene sceneTwo = null;
                try
                {
                    sceneTwo = new Scene(FXMLLoader.load(getClass().getResource("/Windows/tables.fxml")));
                    Main.pStage.setScene(sceneTwo);
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }

            }

        });



    }

    private void loginUserError() {
        user_box.setText("");
        pass_box.setText("");
        user_box.getStyleClass().add("wrong-credentials");
        pass_box.getStyleClass().add("wrong-credentials");
    }
}
