/**
 * Sample Skeleton for 'adminPane.fxml' Controller Class
 */

package com.Controllers;

import com.jfoenix.controls.*;

import java.net.URL;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

import com.jfoenix.controls.events.JFXDialogEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ToggleGroup;
import javafx.scene.effect.BoxBlur;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

import static com.conf.DataBaseHandler.getDbConn;

public class ControllerAdmin {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="rootPane"
    private StackPane rootPane; // Value injected by FXMLLoader

    @FXML // fx:id="rootAnchore"
    private AnchorPane rootAnchore; // Value injected by FXMLLoader

    @FXML // fx:id="change"
    private MenuItem change; // Value injected by FXMLLoader

    @FXML // fx:id="exit"
    private MenuItem exit; // Value injected by FXMLLoader

    @FXML // fx:id="about"
    private MenuItem about; // Value injected by FXMLLoader

    @FXML // fx:id="login"
    private JFXTextField login; // Value injected by FXMLLoader

    @FXML // fx:id="password"
    private JFXPasswordField password; // Value injected by FXMLLoader

    @FXML // fx:id="admin"
    private JFXRadioButton admin; // Value injected by FXMLLoader

    @FXML // fx:id="users"
    private ToggleGroup users; // Value injected by FXMLLoader

    @FXML // fx:id="prepod"
    private JFXRadioButton prepod; // Value injected by FXMLLoader

    @FXML // fx:id="reg"
    private JFXButton reg; // Value injected by FXMLLoader

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
       reg.setOnAction(event -> {
           String role = null;
           if(admin.isSelected()){
               role = "admin";
           } else {
               role = "teacher";
           }

           Statement st;
            String user ="select c_users('"+login.getText().trim()+"','"+password.getText().trim()+"','"+role+"');";
           try {
               st = getDbConn().createStatement();
               st.execute(user);
               dialog("Выполнено!","Вы успешно добавили нового пользователя.");
               login.clear();
               password.clear();
           } catch (SQLException e) {
               e.printStackTrace();
               dialog("Ошибка!","Ошибка во время добавления пользователя.");
           }
       });

    }
    private void dialog(String heading,String body){
        BoxBlur blur = new BoxBlur(3, 3, 3);

        JFXDialogLayout dialogLayout = new JFXDialogLayout();
        dialogLayout.setHeading(new Text(heading));
        dialogLayout.setBody(new Text(body));
        JFXButton btn = new JFXButton("ОК");
        btn.getStyleClass().add("dialog-button");
        JFXDialog dialog = new JFXDialog(rootPane, dialogLayout, JFXDialog.DialogTransition.TOP);
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialog.close();
            }
        });
        dialogLayout.setActions(btn);
        dialog.show();
        dialog.setOnDialogClosed((JFXDialogEvent event)->{
            rootAnchore.setEffect(null);
        });
        rootAnchore.setEffect(blur);
    }
}
