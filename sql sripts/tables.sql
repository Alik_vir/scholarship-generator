-- CREATE TYPE gender AS ENUM ('M', 'F');

/******************/
\c postgres
drop database lar;
create database lar;
\c lar
/******************/


CREATE TABLE specialnosti ( --заполнять надо
    id_spec varchar(10) PRIMARY KEY not null,
    name_spec varchar(64) not null CHECK (name_spec <> ''),
    time_tutorial varchar(64) not null CHECK (time_tutorial <> '')
    );

CREATE TABLE groups ( --заполнять надо
    id_group varchar(15) PRIMARY KEY, --можно что бы было вводить ПКС-303 и т.д.
    id_spec varchar(10) REFERENCES specialnosti ON DELETE CASCADE ON UPDATE CASCADE
    );

CREATE TABLE discipline ( --заполнять надо
    id_discipline int PRIMARY KEY  not null generated always as identity (start with 1 increment by 1),
    name_discipline varchar(64) not null CHECK(name_discipline <> '')
    );

CREATE TABLE students ( --студенты
    id_student int PRIMARY KEY not null generated always as identity (start with 1 increment by 1),
    num_jurnal int NOT NULL CHECK (num_jurnal > 0),
    surname varchar(64) not null CHECK (surname <> ''),
    name_stud varchar(64) not null CHECK (name_stud <> ''),
    sex varchar(1) not null CHECK (sex <> ''),
    birthday timestamp without time zone not null CHECK (now() > birthday),
    phone varchar(20) not null CHECK (phone <> ''),
    id_group varchar(15) REFERENCES groups on delete cascade on update cascade
    );

CREATE TABLE privileges_stud ( --заполнять надо
    id_pr int PRIMARY KEY not null generated always as identity (start with 1 increment by 1),
    id_st int REFERENCES students on delete cascade on update cascade,
    doc varchar(32) not null CHECK(doc <> ''),
    date_pr timestamp without time zone not null,
    notes varchar(100)
    );

--Сту->Степендии-> ->

CREATE TABLE kinds_stip( --виды стипендий, заполнять надо
    num_stip int PRIMARY KEY not null generated always as identity (start with 1 increment by 1),
    kinds varchar(20) not null CHECK (kinds <> ''),
    size_st numeric not null CHECK (size_st > 0)
    );

drop table uspev cascade;
--странно выглядит, но вроде работает
CREATE TABLE uspev ( --заполнять надо
  id_uspev int PRIMARY KEY not null generated always as identity (start with 1 increment by 1),
  num_semestr int CHECK (num_semestr > 0),
  id_discipline int REFERENCES discipline on delete cascade on update cascade,
  id_student int REFERENCES students on delete cascade on update cascade,
  mark smallint not null CHECK (mark > 0)
);

drop table stp;

CREATE TABLE stp ( --должно рассчитыватся само
  num int PRIMARY KEY not null generated always as identity (start with 1 increment by 1),
  id_student int  REFERENCES students on delete cascade on update cascade,
  num_stip int REFERENCES kinds_stip (num_stip) on delete cascade on update cascade,
  num_semestr int
  );

--Примеры
insert into specialnosti (id_spec, name_spec, time_tutorial) values('09.02.03', 'Программирование в компьютерных сетях', '3 года 10 месяцев');
insert into specialnosti (id_spec, name_spec, time_tutorial) values('09.02.02', 'Компьютерные сети', '3 года 10 месяцев');
insert into specialnosti (id_spec, name_spec, time_tutorial) values('09.02.05', 'Прикладная информатика', '3 года 10 месяцев');
insert into specialnosti (id_spec, name_spec, time_tutorial) values('10.02.03', 'Информационная безопасность автоматизированных систем', '3 года 10 месяцев');
insert into specialnosti (id_spec, name_spec, time_tutorial) values('21.02.05', 'Земельно-имущественные отношения', '2 года 10 месяцев');
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
insert into groups (id_group, id_spec) values ('П-303', '09.02.03');
insert into groups (id_group, id_spec) values ('КС-103', '09.02.02');
insert into groups (id_group, id_spec) values ('ПИ-203', '09.02.05');
insert into groups (id_group, id_spec) values ('ИБ-204', '10.02.03');
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
insert into discipline (name_discipline) values ('Высшая Математика');
insert into discipline (name_discipline) values ('Программирование');
insert into discipline (name_discipline) values ('Защиты баз даннхы');
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
insert into students (num_jurnal, surname, name_stud, sex, birthday, phone, id_group) values (1, 'Петров','Петр','M', '1998-10-10', '+79776508536', 'П-303');
insert into students (num_jurnal, surname, name_stud, sex, birthday, phone, id_group) values (5, 'Алексеев','Максим','M', '1998-10-10', '+79776508536', 'П-303');
insert into students (num_jurnal, surname, name_stud, sex, birthday, phone, id_group) values (2, 'Прошломыв','Петр','M', '1998-10-10', '+79776508536', 'КС-103');
insert into students (num_jurnal, surname, name_stud, sex, birthday, phone, id_group) values (3, 'Петров','Петр','M', '1998-10-10', '+79776508536', 'ПИ-203');
insert into students (num_jurnal, surname, name_stud, sex, birthday, phone, id_group) values (4, 'Виолета','Виолетовна','M', '1998-10-10', '+79776508536', 'ИБ-204');
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
insert into kinds_stip (kinds, size_st) values ('Повышенная', 3000);
insert into kinds_stip (kinds, size_st) values ('Обычная', 1000);
insert into kinds_stip (kinds, size_st) values ('Льготная', 4000);

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
insert into uspev (num_semestr, id_discipline, id_student, mark) values (1, 1, 1, 5);
insert into uspev (num_semestr, id_discipline, id_student, mark) values (1, 2, 1, 5);
insert into uspev (num_semestr, id_discipline, id_student, mark) values (1, 1, 2, 3);
insert into uspev (num_semestr, id_discipline, id_student, mark) values (1, 2, 2, 3);
insert into uspev (num_semestr, id_discipline, id_student, mark) values (1, 1, 3, 3);
insert into uspev (num_semestr, id_discipline, id_student, mark) values (1, 2, 3, 3);
insert into uspev (num_semestr, id_discipline, id_student, mark) values (1, 1, 4, 4);
insert into uspev (num_semestr, id_discipline, id_student, mark) values (1, 2, 4, 5);
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
insert into privileges_stud (id_st, doc, date_pr) values (2, 'passport', '2019-10-01');


-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
