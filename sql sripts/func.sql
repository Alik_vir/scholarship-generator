

/*
??????????????????????????????????
- В функции добовления в таблицу stp(степух) сделать проверку на то что id студента и id еместра не будут повторятся
Но если это будет автоматически заполнятся, то можно не делать.
*/

--VIEW TABLES
CREATE FUNCTION l_specialnosti() returns SETOF specialnosti
AS $$
      select id_spec,
              name_spec,
              time_tutorial
      FROM specialnosti;
$$ LANGUAGE sql SECURITY DEFINER;

CREATE FUNCTION l_groups() returns SETOF groups
AS $$
      select id_group,
              id_spec
      FROM groups;
$$ LANGUAGE sql SECURITY DEFINER;

CREATE FUNCTION l_disciplin() returns SETOF discipline
AS $$
      select id_discipline,
             name_discipline
      FROM discipline;
$$ LANGUAGE sql SECURITY DEFINER;

CREATE FUNCTION l_students() returns SETOF students
AS $$
      select  id_student,
              num_jurnal,
              surname,
              name_stud,
              sex,
              birthday,
              phone,
              id_group
      FROM students;
$$ LANGUAGE sql SECURITY DEFINER;

CREATE FUNCTION l_privileges() returns SETOF privileges_stud
AS $$
      select id_pr,
              id_st,
              doc,
              date_pr,
              notes
      FROM privileges_stud;
$$ LANGUAGE sql SECURITY DEFINER;

CREATE FUNCTION l_kinds_stip() returns SETOF kinds_stip
AS $$
      select  num_stip,
              kinds,
              size_st
      FROM kinds_stip;
$$ LANGUAGE sql SECURITY DEFINER;

CREATE FUNCTION l_uspev() returns SETOF uspev
AS $$
      select  id_uspev,
              num_semestr,
              id_discipline,
              id_student,
              mark
      FROM uspev;
$$ LANGUAGE sql SECURITY DEFINER;


CREATE FUNCTION l_stp() returns SETOF stp
AS $$
      Select  num,
              id_student,
              num_semestr,
              num_semestr
      From stp;
$$ LANGUAGE sql SECURITY DEFINER;

--FUNCTIONS
--SPECIALNOSTI
--добавление специальности
CREATE FUNCTION C_spec(id_new varchar(64),name_new varchar(64),time_new varchar(64)) RETURNS int
AS $$
	begin
		INSERT INTO specialnosti (id_spec,name_spec, time_tutorial) values (id_new, name_new, time_new);
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--удаление специальности
CREATE or replace FUNCTION D_spec(id_new varchar) RETURNS int
AS $$
	begin
		DELETE FROM specialnosti WHERE id_spec = id_new;
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--изменение специальности
CREATE FUNCTION E_spec (id_new varchar(64),name_new varchar(64),time_new varchar(64)) RETURNS int
AS $$
	begin
		UPDATE specialnosti SET id_spec = id_new, name_spec = name_new, time_tutrial = time_new WHERE id_spec = id_spec_inp;
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;


--GROUPS
--добавление групп
CREATE FUNCTION C_groups(group_new varchar, spec_new varchar) RETURNS int
AS $$
	begin
		INSERT INTO groups (id_group, id_spec) values (group_new, spec_new);
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--удаление групп
CREATE FUNCTION D_groups(group_new varchar) RETURNS int
AS $$
	begin
		DELETE FROM groups WHERE id_group = group_new;
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--изменение групп
CREATE FUNCTION E_grops(group_new int, spec_new int) RETURNS int
AS $$
	begin
		UPDATE groups SET id_group = group_new, id_spec = spec_new WHERE id_group = group_new;
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--DISCIPLIN
--добавление дисциплин
CREATE FUNCTION C_discipline(name_disciplin_inp varchar(64)) RETURNS int
AS $$
	begin
		INSERT INTO discipline (name_discipline) values (name_disciplin_inp);
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--удаление дисциплин
CREATE FUNCTION D_disc(id_disciplin_inp int) RETURNS int
AS $$
	begin
		DELETE FROM discipline WHERE id_discipline = id_disciplin_inp;
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--изменение дисциплин
CREATE FUNCTION E_discipline(id_disciplin_inp int, name_disciplin_inp varchar(64)) RETURNS int
AS $$
	begin
		UPDATE disciplin SET name_disciplin = name_disciplin_inp
        WHERE id_disciplin = id_disciplin_inp;
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--STUDENTS

--добавление студента
CREATE FUNCTION C_students(jurnal_new int, surname_inp varchar(64), name_stud_inp varchar(64), sex_inp varchar(1), birthday_inp timestamp without time zone, phone_inp varchar(20), id_group_inp varchar) RETURNS int
AS $$
	begin
		INSERT INTO students (num_jurnal, surname, name_stud, sex, birthday, phone, id_group) values (jurnal_new, surname_inp, name_stud_inp, sex_inp, birthday_inp, phone_inp, id_group_inp);
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--удаление студента
CREATE FUNCTION D_students(id_student_inp int) RETURNS int
AS $$
	begin
		DELETE FROM students WHERE id_student = id_student_inp;
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--изменение студента
CREATE FUNCTION E_students(id_student_inp int, jurnal_new int, surname_inp varchar(64), name_stud_inp varchar(64), sex_inp varchar, birthday_inp timestamp without time zone, phone_inp varchar(20), id_group_inp int) RETURNS int
AS $$
	begin
		UPDATE students SET num_jurnal = jurnal_new, surname = surname_inp, name_stud = name_stud_inp, sex = sex_inp, birthday = birthday_inp, phone = phone_inp, id_group = id_group_inp WHERE id_student = id_student_inp;
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--PRIVILEGES_STUD
--добавление льгот
CREATE FUNCTION C_privil(st_new int, doc_new varchar(32), date_new timestamp without time zone, notes_new varchar(100)) RETURNS int
AS $$
declare
	result int;
	begin
		INSERT INTO privileges_stud (id_st, doc, date_pr, notes) values (st_new, doc_new, date_new, notes_new);
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--удаление льгот
CREATE FUNCTION D_privil(id_privileges_inp int) RETURNS int
AS $$
	begin
		DELETE FROM privileges_stud WHERE id_pr = id_privileges_inp;
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--изменение льгот
CREATE FUNCTION E_privil (pr_new int, st_new int, doc_new varchar(32), date_new timestamp without time zone, notes_new varchar(100)) RETURNS int
AS $$
	begin
		UPDATE privileges_stud SET id_student = id_student_inp, documents_for_privileges = documents_for_privileges_inp, date_privilages = date_privilages_inp  WHERE id_privileges = id_privileges_inp;
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--KINDS_STIP
--добавление стипух
CREATE FUNCTION C_stip(kinds_inp varchar(20), size_st_inp numeric) RETURNS int
AS $$
	begin
		INSERT INTO kinds_stip (kinds, size_st) values (kinds_inp, size_st_inp);
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--удаление стипух
CREATE FUNCTION D_stip(stip_new int) RETURNS int
AS $$
	begin
		DELETE FROM kinds_stip WHERE num_stip = stip_new;
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--изменение стипух
CREATE FUNCTION E_stip(stip_new int, kinds_inp varchar(20), size_st_inp numeric) RETURNS int
AS $$
	begin
		UPDATE kinds_stip SET kinds = kinds_inp, size_st = size_st_inp  WHERE num_stip = stip_new;
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;


CREATE FUNCTION c_uspev(sem_new int, disc_new int, st_new int, mark_new int) RETURNS int
AS $$
    begin
        if exists (Select num_semestr, id_discipline, id_student from uspev where num_semestr = sem_new and id_discipline = disc_new and id_student = st_new) then raise exception 'problema';
        else
        insert into uspev (num_semestr, id_discipline, id_student, mark) values (sem_new, disc_new, st_new, mark_new);
        end if;
    return 0;
    end;
$$ LANGUAGE plpgsql SECURITY DEFINER; -- готовая есть ограничитель

CREATE FUNCTION d_uspev (uspev_new int) returns int
AS $$
    begin
        DELETE from uspev where id_uspev = uspev_new;
    return 0;
    end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

CREATE FUNCTION e_uspev(id_new int, sem_new int, disc_new int, st_new int, mark_new int) RETURNS int
AS $$
    begin
    if exists (Select num_semestr, id_discipline, id_student from uspev where num_semestr = sem_new and id_discipline = disc_new and id_student = st_new) then raise exception 'problema';
    else
        UPDATE uspev set num_semestr = sem_new, id_disciplin = disc_new, id_student = st_new, mark = mark_new where id_uspev = id_new;
    end if;
    return 0;
    end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

----------------------------------------------------------------------------------------------------
--представление для успеваемости
CREATE VIEW gooduspev AS
      select uspev.id_uspev, students.surname, students.name_stud, students.sex, students.id_group, uspev.num_semestr, discipline.name_discipline, uspev.mark
        FROM students, uspev, discipline
        WHERE students.id_student = uspev.id_student AND
        uspev.id_discipline = discipline.id_discipline;

        select * from gooduspev;

CREATE VIEW goodprivileges AS
      select privileges_stud.id_pr, students.surname, students.name_stud, students.id_group, privileges_stud.doc, privileges_stud.date_pr, privileges_stud.notes
      from privileges_stud, students
      where privileges_stud.id_st = students.id_student;

      select * from goodprivileges;

drop view goodstp;

CREATE VIEW goodstp AS
      select stp.num, students.num_jurnal, students.surname, students.name_stud, students.sex, students.id_group, kinds_stip.kinds, kinds_stip.size_st, stp.num_semestr
      FROM students, kinds_stip, stp
      WHERE stp.id_student = students.id_student AND stp.num_stip = kinds_stip.num_stip;

      select * from goodstp;




--семестр вводишь в функцию при вызове
drop function step_gen();

CREATE FUNCTION step_gen(num_semestr_into int) RETURNS int
AS $$
    begin
        Delete from stp;
        if exists(select id_student from students where id_student IN (select id_student from privileges_stud)) then

        INSERT INTO stp (num_stip, num_semestr, id_student) SELECT 4, num_semestr_into, id_student from students
        where ((select MIN(mark) from uspev where id_student = students.id_student AND num_semestr = num_semestr_into) >= 4);

        end if;

        INSERT INTO stp (num_stip, num_semestr, id_student) SELECT 2, num_semestr_into, id_student from students
        where ((select MIN(mark) from uspev where id_student = students.id_student AND num_semestr = num_semestr_into) >= 4);


        INSERT INTO stp (num_stip, num_semestr, id_student) SELECT 1, num_semestr_into, id_student from students
        where ((select MIN(mark) from uspev where id_student = students.id_student AND num_semestr = num_semestr_into) >= 5);
              return 0;
    end;
$$ LANGUAGE plpgsql SECURITY DEFINER;





CREATE FUNCTION C_users(user_inp varchar, password_inp varchar, role_user varchar) RETURNS int
AS $$
	begin
	execute 'create user ' || user_inp || ' with password ''' || password_inp || ''' in role '|| role_user || ';';
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;

--удаление юзера
CREATE FUNCTION D_users(user_inp varchar) RETURNS int
AS $$
	begin
	execute 'drop user ' || user_inp || ';';
		return 0;
	end;
$$ LANGUAGE plpgsql SECURITY DEFINER;



--запрос на студентов со стипухой - работает
Select surname, name_stud, sex from students as T1 where (select MIN(mark) from uspev where id_student = T1.id_student) > 3;

--запрос на льготников
select surname, name_stud, sex from students where id_student IN (select id_student from privileges_stud);
