create or replace function stp_generate() returns integer
AS $$
    declare
    cur cursor for select num_semestr, id_disciplin, id_student, mark from uspev;

    rec record;

BEGIN
  open cur;
    loop
        fetch cur into rec;
    if not found then
        insert into stp(id_student, num_step, num_semestr) values (rec.id_student, 1, rec.num_semestr);
        exit;
  end if;
  end loop;
  close cur;
  return 0;
end;
$$ language plpgsql;

select stp_generate();
select * from stp;
